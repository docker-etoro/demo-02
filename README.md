# Docker Workshop
Demo 02: Understanding the Docker Components

---

## Instructions

 - Access to the grafana repository:
```
https://hub.docker.com/r/grafana/grafana
```

 - Show the repository image tags:
```
https://hub.docker.com/r/grafana/grafana/tags/
```

 - Go to the terminal and check the existent images using:
```
$ docker images
```

 - Pull the version 6.7.3 with:
```
$ docker pull grafana/grafana:6.7.3
```

 - Show the existent images and look for the pulled image:
```
$ docker images
```

 - Show the current running containers using:
```
$ docker ps
```

 - Create a new container from the pulled image:
```
$ docker run -d -p 3000:3000 --name grafana grafana/grafana:6.7.3
```

 - Check if the new container is running now:
```
$ docker ps
```

 - Inspect the container logs:
```
$ docker logs -f grafana
```

 - Browse to the application:
```
http://server-ip:3000/
```

 - Clean:
```
$ docker rm -f grafana
```
